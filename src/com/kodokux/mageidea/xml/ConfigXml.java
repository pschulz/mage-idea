package com.kodokux.mageidea.xml;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/11
 * Time: 20:21
 */
public class ConfigXml {

    private final Document document;
    private final File myConfigFile;

    private String moduleName;
    private String version;
    private String codePoll;
    private List<Depends> depends = new ArrayList<Depends>();


    private HashMap<String, String> packageXpaths = new HashMap<String, String>();

    {
        packageXpaths.put("moduleName", "/config/modules/*[1]");
        packageXpaths.put("version", "/config/modules//version[1]");
        packageXpaths.put("codePool", "/config/modules//codePool[1]");
        packageXpaths.put("depends", "/config/modules//depends[1]");
    }


    public ConfigXml(File configFile) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        document = builder.build(configFile);
        myConfigFile = configFile;
    }

    public void load() {
        Element el = getNode(packageXpaths.get("moduleName"));
        if (el != null) {
            moduleName = el.getName();
        }

        el = getNode(packageXpaths.get("version"));
        if (el != null) {
            version = el.getValue();
        }
    }

    public void load(String name) {
        Element el = getNode("/config/modules/" + name + "[1]");
        if (el != null) {
            moduleName = el.getName();
        }

        el = getNode("/config/modules/" + name + "[1]/version[1]");
        if (el != null) {
            version = el.getValue();
        }

        el = getNode("/config/modules/" + name + "[1]/codePool[1]");
        if (el != null) {
            codePoll = el.getValue();
        }

        el = getNode("/config/modules/" + name + "[1]/depends");
        if (el != null) {
            for (Element depend : el.getChildren()) {
                Depends dependsObj = new Depends();
                dependsObj.setPackageName(depend.getName());
                depends.add(dependsObj);
            }
        }

    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    private Element getNode(String xpath) {
        Element target = null;
        try {
            XPath xpathExpression = XPath.newInstance(xpath);
            target = (Element) xpathExpression.selectSingleNode(getDocument());
        } catch (JDOMException e) {
        }
        return target;
    }

    public List<Depends> getDepends() {
        return depends;
    }

    public Document getDocument() {
        return document;
    }

    public String getCodePoll() {
        return codePoll;
    }

    public void setCodePoll(String codePoll) {
        this.codePoll = codePoll;
    }

    public class Depends {
        private String packageName = "";
        private String channel = "";
        private String min = "";
        private String max = "";

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getMin() {
            return min;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }
    }

}
