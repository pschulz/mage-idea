package com.kodokux.mageidea.xml;

import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.vfs.VirtualFile;
import io.netty.util.internal.StringUtil;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 20:27
 */
public class ConfigXmlUtil {
    static public String getExtensionName(VirtualFile virtualFile) {
        Element el = null;
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            el = (Element) XPath.newInstance("/config/modules").selectSingleNode(build);

            if (el != null) {
                List<Element> children = el.getChildren();

                for (int i = 0; i < children.size(); i++) {
                    return children.get(i).getName();
                }
            }

        } catch (JDOMException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static String getExtensionVersion(VirtualFile virtualFile) {
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            Element el = (Element) XPath.newInstance("/config/modules/*/version").selectSingleNode(build);
            if (el != null)
                return el.getText();
        } catch (JDOMException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static String getExtensionModelShortName(VirtualFile virtualFile) {
        Element el = null;
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            el = (Element) XPath.newInstance("/config/global/models").selectSingleNode(build);

            if (el != null) {
                List<Element> children = el.getChildren();

                for (int i = 0; i < children.size(); i++) {
                    return children.get(i).getName();
                }
            }

        } catch (JDOMException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static Element getElementIfNotExistCreate(VirtualFile virtualFile, String xpath, Element def) throws JDOMException, IOException {
        Document build = new SAXBuilder().build(virtualFile.getUrl());

        Element rootElement = build.getRootElement();

        xpath = xpath.replaceFirst("/", "");
        String[] split = StringUtil.split(xpath, '/');

        if (rootElement == null) {
            rootElement = new Element(split[0]);
        }

        Element n = rootElement;
        for (int i = 1; i < split.length; i++) {
            List<Element> kids = n.getChildren();
            Element nfound = null;
            for (int j = 0; j < kids.size(); j++)
                if (kids.get(j).getName().equals(split[i])) {
                    nfound = kids.get(j);
                    break;
                }
            if (nfound == null) {
                nfound = new Element(split[i]);
                n.addContent(nfound);
            }
            n = nfound;
        }

        List<Element> kids = n.getChildren();
        if (kids.size() > 0) {
            int size = kids.size();
            for (int i = 0; i < size; i++) {
                if (kids.get(i).getName().equals(def.getName())) {
                    if (def.getChildren().size() > 0) {
                        kids.get(i).addContent(def);
                    } else {
                        kids.get(i).setText(def.getText());
                    }
                } else {
                    n.addContent(def);
                }
            }
        } else {
            n.addContent(def);
        }

        return rootElement;
    }


    public static String getExtensionHelperShortName(VirtualFile virtualFile) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public static String getExtensionResourceModel(VirtualFile virtualFile) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public static Event[] getEvents(VirtualFile virtualFile) {
        List list = null;
        List<Event> returnList = new ArrayList<Event>();
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            list = XPath.newInstance("/config//events").selectNodes(build);

            for (Object el : list) {
                Element events = (Element) el;
                List<Element> children = events.getChildren();

                Element parent = (Element) events.getParent();
                if (parent != null) {
                    final String depth = parent.getName();

                    if (children != null && children.size() > 0) {
                        Element firstChild = children.get(0);
                        final String eventName = firstChild.getName();
                        final Element className = (Element) XPath.newInstance("//" + depth + "//" + eventName + "//class").selectSingleNode(firstChild);
                        final Element method = (Element) XPath.newInstance("//" + depth + "//" + eventName + "//method").selectSingleNode(firstChild);

                        Event event = new Event() {
                            @Override
                            public String getEventType() {
                                return depth;
                            }

                            @Override
                            public String getEventName() {
                                return eventName;
                            }

                            @Override
                            public String getClassName() {
                                return className.getText();
                            }

                            @Override
                            public String getMethod() {
                                return method.getText();
                            }
                        };

                        returnList.add(event);

                    }
                }
            }

        } catch (JDOMException e) {
        } catch (IOException e) {
        }

        return returnList.toArray(new Event[returnList.size()]);
    }

    public static Rewrite[] getRewrites(VirtualFile virtualFile) {
        List<Rewrite> returnList = new ArrayList<Rewrite>();

        List list = null;
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            list = XPath.newInstance("/config/global//rewrite").selectNodes(build);

            for (Object el : list) {
                Element events = (Element) el;
                List<Element> children = events.getChildren();

                Element parent = (Element) events.getParent();
                if (parent != null && children != null && children.size() > 0) {
                    Element parent1 = (Element) parent.getParent();
                    if (parent1 != null) {
                        final String rewriteType = parent1.getName();
                        final String tagName = parent.getName();
                        final String rewriteClassShortName = children.get(0).getName();
                        final String rewriteClass = children.get(0).getText();


                        Rewrite rewrite = new Rewrite() {
                            @Override
                            public String getRewriteType() {
                                return rewriteType;
                            }

                            @Override
                            public String getTagName() {
                                return tagName;
                            }

                            @Override
                            public String getRewriteClassShortName() {
                                return rewriteClassShortName;
                            }

                            @Override
                            public String getRewriteClass() {
                                return rewriteClass;
                            }
                        };

                        returnList.add(rewrite);

                    }
                }
            }


        } catch (JDOMException e) {
        } catch (IOException e) {
        }

        return returnList.toArray(new Rewrite[returnList.size()]);
    }

    public static OverloadController[] getOverloadController(VirtualFile virtualFile) {
        List<OverloadController> returnList = new ArrayList<OverloadController>();

        List list = null;
        try {
            Document build = new SAXBuilder().build(virtualFile.getUrl());
            list = XPath.newInstance("/config//routers//modules").selectNodes(build);

            for (Object el : list) {
                Element events = (Element) el;
                List<Element> children = events.getChildren();

                Element parent = (Element) events.getParent();
                if (parent != null && children != null && children.size() > 0) {
                    Element parent1 = (Element) parent.getParent();
                    if (parent1 != null && parent1.getParent() != null) {
                        final String rewriteType = ((Element) parent1.getParent()).getName();
                        final String routeName = parent1.getName();
                        Element moduleElement = children.get(0);
                        final String nameSpace = moduleElement.getName();
                        final String before = moduleElement.getAttributeValue("before");
                        final String rewriteClass = children.get(0).getText();


                        OverloadController overloadController = new OverloadController() {
                            @Override
                            public String getType() {
                                return rewriteType;
                            }

                            @Override
                            public String getRootName() {
                                return routeName;
                            }

                            @Override
                            public String getNameSpace() {
                                return nameSpace;
                            }

                            @Override
                            public String getBefore() {
                                return before;
                            }

                            @Override
                            public String getClassName() {
                                return rewriteClass;
                            }
                        };

                        returnList.add(overloadController);

                    }
                }
            }


        } catch (JDOMException e) {
        } catch (IOException e) {
        }

        return returnList.toArray(new OverloadController[returnList.size()]);
    }

    public static void addRewride(Rewrite rewrite, VirtualFile virtualFile) throws JDOMException, IOException {
        Document build = new SAXBuilder().build(virtualFile.getUrl());


        Element rewriteElement = new Element(rewrite.getRewriteClassShortName());
        rewriteElement.addContent(rewrite.getRewriteClass());
        Element elementIfNotExistCreate = getElementIfNotExistCreate(virtualFile, "/config/global/" + rewrite.getRewriteType() + "/" + rewrite.getTagName() + "/rewrite", rewriteElement);


        XMLOutputter xmlOutput = new XMLOutputter();
        Format format = Format.getRawFormat();
        format.setEncoding("UTF-8");
        format.setLineSeparator("\n");
        xmlOutput.setFormat(format);
//
//
////        com.intellij.openapi.editor.Document document = FileDocumentManager.getInstance().getDocument(virtualFile);
////        document.get
////        virtualFile.se
//
////        docManager.commitDocument();
////        // display nice nice
        com.intellij.openapi.editor.Document document = FileDocumentManager.getInstance().getDocument(virtualFile);
        if (document != null) {
            document.setText(xmlOutput.outputString(elementIfNotExistCreate));
        }
//        EditorFactory.getInstance().createDocument()
////
//        xmlOutput.output(elementIfNotExistCreate, virtualFile.getOutputStream(null));


        return;
    }

    public interface Rewrite {

        String getRewriteType();

        String getTagName();

        String getRewriteClassShortName();

        String getRewriteClass();
    }

    public interface Event {

        String getEventType();

        String getEventName();

        String getClassName();

        String getMethod();
    }

    public interface OverloadController {

        String getType();

        String getRootName();

        String getNameSpace();

        String getBefore();

        String getClassName();
    }
}
