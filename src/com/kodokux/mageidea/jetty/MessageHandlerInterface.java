package com.kodokux.mageidea.jetty;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/18
 * Time: 21:12
 */
public interface MessageHandlerInterface {
    public void onMessage(String message);
}
