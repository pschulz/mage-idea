package com.kodokux.mageidea.jetty;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/18
 * Time: 16:38
 */

@WebSocket
public class MyWebSocket {
    private RemoteEndpoint remote;
    private MessageHandlerInterface messageHandlerInterface;

    public MyWebSocket(MessageHandlerInterface messageHandlerInterface) {
        this.messageHandlerInterface = messageHandlerInterface;
    }

    public MyWebSocket() {
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("O Socket foi aberto!!!!!");
        this.remote = session.getRemote();
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
        System.out.println("Cliente mandou a menssagem: " + message);
        showMessage(message);

        try {
            remote.sendString("Oi como vai!?");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("WebSocket Fechou!!. Code:" + statusCode);
    }

    private void showMessage(String message) {
        messageHandlerInterface.onMessage(message);
    }

}