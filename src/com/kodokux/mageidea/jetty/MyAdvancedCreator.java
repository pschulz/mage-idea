package com.kodokux.mageidea.jetty;

import org.eclipse.jetty.websocket.api.UpgradeRequest;
import org.eclipse.jetty.websocket.api.UpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/18
 * Time: 19:55
 */
public class MyAdvancedCreator implements WebSocketCreator {

    private final MessageHandlerInterface messageHandlerInterface;

    public MyAdvancedCreator() {
        this.messageHandlerInterface = null;
    }

    public MyAdvancedCreator(MessageHandlerInterface project) {
        this.messageHandlerInterface = project;
    }


    @Override
    public Object createWebSocket(UpgradeRequest upgradeRequest, UpgradeResponse upgradeResponse) {
        return new MyWebSocket(messageHandlerInterface);
    }
}
