package com.kodokux.mageidea.module;

import com.intellij.openapi.project.Project;
import com.kodokux.mageidea.service.Magento;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 19:16
 */
public class Extension {
    private static Document DOCUMENT = null;
    private String name;
    private final File configFile;
    private File rootPath;
    private String channel;

    public Extension(@NotNull String name, String channel, Project project) {
        Magento magento = Magento.getInstance(project);
        this.rootPath = magento.getPathByExtensionName(name, channel);
        this.name = name;
        this.configFile = new File(rootPath, "etc/config.xml");
    }

    public String getExtensionName() {
        if (name == null) {
            name = getNameByConfig();
        }
        return name;
    }

    public String getModuleName() {
        String extensionName = getExtensionName();
        String[] split = extensionName.split("_");
        if (split.length == 2) {
            return split[1];
        }
        return extensionName;
    }

    public File getRootPath() {
        return rootPath;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public File getConfigFile() {
        return this.configFile;
    }

    public Document getConfigDocument() throws JDOMException, IOException {
        if (DOCUMENT == null) {
            DOCUMENT = new SAXBuilder().build(this.configFile.getPath());
        }
        return DOCUMENT;
    }

    public String getExtensionModelTagName() {
        Element el = null;
        try {
            Document configDocument = this.getConfigDocument();
            el = (Element) XPath.newInstance("/config/global/models").selectSingleNode(configDocument);
            if (el != null) {
                List<Element> children = el.getChildren();
                for (int i = 0; i < children.size(); i++) {
                    return children.get(i).getName();
                }
            }
        } catch (JDOMException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public String getNameByConfig() {
        Element el = null;
        try {
            Document configDocument = this.getConfigDocument();
            el = (Element) XPath.newInstance("/config/modules").selectSingleNode(configDocument);
            if (el != null) {
                List<Element> children = el.getChildren();
                for (int i = 0; i < children.size(); i++) {
                    return children.get(i).getName();
                }
            }
        } catch (JDOMException e) {
        } catch (IOException e) {
        }
        return null;
    }


    @Override
    public String toString() {
        return channel + "  ( " + name + " )";
    }
}
