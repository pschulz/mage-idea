package com.kodokux.mageidea.action;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Ref;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.PathUtil;
import com.jetbrains.php.PhpIcons;
import com.jetbrains.php.templates.PhpFileTemplateUtil;
import com.kodokux.mageidea.service.Magento;
import com.kodokux.mageidea.templates.MagentoFileFromTemplateDataProvider;
import com.kodokux.mageidea.xml.ConfigXmlUtil;
import org.jdom.JDOMException;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 18:04
 */
public abstract class NewBaseAction extends AnAction {
    protected VirtualFile _virtualFile;
    private DataContext _dataContext;
    private EditorEx event;

    public NewBaseAction(String actionName, String description) {
        this(actionName, description, PhpIcons.PHP_FILE);
    }

    public NewBaseAction(String actionName, String description, Icon phpFile) {
        super(actionName, description, phpFile);
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = (IdeView) LangDataKeys.IDE_VIEW.getData(dataContext);
        if (view == null)
            return;
        Project project = (Project) PlatformDataKeys.PROJECT.getData(dataContext);
        if (project == null)
            return;
        PsiDirectory initialBaseDir = view.getOrChooseDirectory();
        if (initialBaseDir == null) {
            return;
        }

        VirtualFile file = e.getData(PlatformDataKeys.VIRTUAL_FILE);
        if (file == null) {
            return;
        }

        Editor editor = e.getData(PlatformDataKeys.EDITOR);
        if (editor == null) {
            return;
        }

        PsiFile file1 = PsiManager.getInstance(project).findFile(file);
        if (file1 == null) {
            return;
        }

        invoke(project, initialBaseDir, file1, editor, view);
    }

    public void invoke(final Project project, PsiDirectory initialBaseDir, PsiFile file, Editor editor, IdeView view) {

        MagentoFileFromTemplateDataProvider dataProvider = getDataProvider(project, initialBaseDir, editor, file);
        if (dataProvider == null)
            return;

        final PsiDirectory baseDir = dataProvider.getBaseDirectory();
        final String templateName = dataProvider.getTemplateName();
        final String filePath = dataProvider.getFilePath();
        final Properties properties = dataProvider.getProperties(baseDir);
        final VirtualFile configXml = dataProvider.getConfigXml();
        final ConfigXmlUtil.Rewrite rewrite = dataProvider.getRewrite();

        properties.setProperty("FILE_NAME", PathUtil.getFileName(filePath));
        final Ref fileRef = new Ref();
        final Ref exceptionRef = new Ref();
        final Magento magento = Magento.getInstance(project);
        CommandProcessor.getInstance().executeCommand(new Runnable() {
            @Override
            public void run() {

                Runnable run = new Runnable() {
                    public void run() {
                        List path = StringUtil.split(filePath.replace(File.separator, "/"), "/");
                        String fileName = (String) path.get(path.size() - 1);

//                        System.out.println(fileName);

                        try {
                            fileRef.set(PhpFileTemplateUtil.createPhpFileFromInternalTemplate(project, baseDir, templateName, properties, fileName));
                        } catch (IOException e) {
                            exceptionRef.set(e);
                            return;
                        }

                        PsiDirectory parentDirectory = baseDir;
                        try {
                            int i = 0;
                            for (int pathSize = path.size() - 1; i < pathSize; i++) {
                                String subDirectoryName = (String) path.get(i);
                                PsiDirectory subDirectory = parentDirectory.findSubdirectory(subDirectoryName);
                                if (subDirectory != null)
                                    parentDirectory = subDirectory;
                                else
                                    parentDirectory = parentDirectory.createSubdirectory(subDirectoryName);
                            }

                            System.out.println("shortTag" + rewrite.getTagName());

                            ConfigXmlUtil.addRewride(rewrite, configXml);


                        } catch (IncorrectOperationException e) {
                            exceptionRef.set(e);
                            return;
                        } catch (JDOMException e) {
                            exceptionRef.set(e);
                        } catch (IOException e) {
                            exceptionRef.set(e);
                        }

                        try {
                            fileRef.set(parentDirectory.add((PsiElement) fileRef.get()));
                        } catch (IncorrectOperationException e) {
                            exceptionRef.set(e);
                            return;
                        }
                    }
                };

                ApplicationManager.getApplication().runWriteAction(run);

            }
        }, getActionName(), null);


        if (!exceptionRef.isNull()) {
            Messages.showErrorDialog(((IncorrectOperationException) exceptionRef.get()).getMessage(), getActionName());
            return;
        }
        if (fileRef.isNull())
            return;
        if (view != null) {
            view.selectElement((PsiElement) fileRef.get());
            ApplicationManager.getApplication().runWriteAction(new Runnable() {
                @Override
                public void run() {
                    CodeStyleManager codeStyleManager = CodeStyleManager.getInstance(project);
                    codeStyleManager.reformat((PsiElement) fileRef.get());


                    PsiFile psiXmlFile = PsiManager.getInstance(project).findFile(configXml);
                    if (psiXmlFile != null) {
                        codeStyleManager.reformat(psiXmlFile);
                    }

                }
            });
        }

    }

    protected MagentoFileFromTemplateDataProvider getDataProvider(Project project, PsiDirectory initialBaseDir, Editor editor, PsiFile file) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    protected String getActionName() {
        return getTemplatePresentation().getText();
    }


}
