package com.kodokux.mageidea.action;

import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.ide.fileTemplates.FileTemplateUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vcs.VcsDirectoryMapping;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.vcsUtil.VcsFileUtil;
import com.kodokux.mageidea.Icons;
import com.kodokux.mageidea.MagentoComponent;
import com.kodokux.mageidea.action.ui.CreateNewExtensionDialog;
import com.kodokux.mageidea.service.Magento;
import com.kodokux.mageidea.xml.MageConfig;
import git4idea.GitVcs;
import git4idea.commands.Git;
import git4idea.commands.GitCommandResult;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 13:29
 */
public class CreateNewExtensionAction extends AnAction {

    private Project project;
    private MagentoComponent magentoComponent;
    private Magento magento;
    private CreateNewExtensionDialog createNewExtensionDialog;

    public CreateNewExtensionAction() {
        super("Magento Extension", "Create New Extension For Magento", Icons.MAGENTO_ICON_16);


    }

    public void createFileFromTemplate(PsiDirectory directory, String templateName, String fileName, Properties properties) throws Exception {

        final FileTemplate template =
                FileTemplateManager.getInstance().getInternalTemplate(templateName);
        FileTemplateUtil.createFromTemplate(template, fileName, properties, directory);

    }


    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {

        project = anActionEvent.getProject();
        if (project == null) {
            return;
        }

        magentoComponent = MagentoComponent.getInstance(project);
        magento = Magento.getInstance(project);

        //LocalFileSystem.getInstance().findFileByPath(path);

        DataContext context = anActionEvent.getDataContext();
        VirtualFile[] files = PlatformDataKeys.VIRTUAL_FILE_ARRAY.getData(context);
        if (files == null || files.length != 1) {
            return;
        }

        VirtualFile dir = files[0];

        createNewExtensionDialog = new CreateNewExtensionDialog(project);
        createNewExtensionDialog.show();
        if (createNewExtensionDialog.isOK()) {
            String vendorName = createNewExtensionDialog.getVendorName();
            String moduleName = createNewExtensionDialog.getModuleName();
            String channel = createNewExtensionDialog.getChannel();

            String base_dirs = vendorName.toLowerCase() + "_" + moduleName.toLowerCase() + "/app/code/" + channel + "/" + vendorName + "/" + moduleName;

            File file = new File(dir.getPath(), base_dirs);

            //create etc
            try {
                if (!file.exists()) {
                    VfsUtil.createDirectories(file.getPath());

                    String rootPath = vendorName.toLowerCase() + "_" + moduleName.toLowerCase();

                    VirtualFile ioFile = VfsUtil.findFileByIoFile(new File(dir.getPath(), rootPath), true);

                    Git git = ServiceManager.getService(Git.class);
                    GitVcs vcs = GitVcs.getInstance(project);
                    GitCommandResult result = git.init(project, ioFile);
                    if (!result.success()) {

                        return;
                    } else {
//                        ModmanUtils.refreshAndConfigureVcsMappings(project, ioFile, ioFile.getPath());
                    }
                }
                MageConfig config = createEtc(file);
                createHelper(config, file);
                createBlock(config, file);
                createModel(config, file);
                createAdminRouters(config, file);
                createFrontendRouters(config, file);

                createSql(config, file);

                config.save(project);


            } catch (Exception e) {
                e.printStackTrace();
            }

            LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
        }

    }

    public static void refreshAndConfigureVcsMappings(final Project project, final VirtualFile root, final String path) {
        root.refresh(false, false);
        ProjectLevelVcsManager vcs = ProjectLevelVcsManager.getInstance(project);
        final List<VcsDirectoryMapping> vcsDirectoryMappings = new ArrayList<VcsDirectoryMapping>(vcs.getDirectoryMappings());
        VcsDirectoryMapping mapping = new VcsDirectoryMapping(path, GitVcs.getInstance(project).getName());
        for (int i = 0; i < vcsDirectoryMappings.size(); i++) {
            final VcsDirectoryMapping m = vcsDirectoryMappings.get(i);
            if (m.getDirectory().equals(path)) {
                if (m.getVcs().length() == 0) {
                    vcsDirectoryMappings.set(i, mapping);
                    mapping = null;
                    break;
                } else if (m.getVcs().equals(mapping.getVcs())) {
                    mapping = null;
                    break;
                }
            }
        }
        if (mapping != null) {
            vcsDirectoryMappings.add(mapping);
        }
        vcs.setDirectoryMappings(vcsDirectoryMappings);
        vcs.updateActiveVcss();
        VcsFileUtil.refreshFiles(project, Collections.singleton(root));
    }

    private void createSql(MageConfig config, File file) {

        String extesionName = magento.getModuleName(file);
        File helperDir = new File(file, "sql/" + extesionName + "_setup");
        if (!helperDir.exists()) {
            helperDir.mkdirs();
        }
        config.setSql();
    }

    private void createAdminRouters(MageConfig config, File file) {
        File helperDir = new File(file, "controllers/Adminhtml");
        if (!helperDir.exists()) {
            helperDir.mkdirs();
        }
        config.setAdminRoutes();
    }

    private void createFrontendRouters(MageConfig config, File file) throws IOException {
        File helperDir = new File(file, "controllers");
        VfsUtil.createDirectories(helperDir.getPath());
//        if (!helperDir.exists()) {
//            helperDir.mkdir();
//        }
        config.setFrontendRouters();
    }

    private void createModel(MageConfig config, File file) throws IOException {
        File helperDir = new File(file, "Model/Resource");
//        if (!helperDir.exists()) {
//            helperDir.mkdirs();
//        }
//        VfsUtil.get
        VfsUtil.createDirectories(helperDir.getPath());
        config.setModels();
    }

    private void createBlock(MageConfig config, File file) {
        File helperDir = new File(file, "Block");
        if (!helperDir.exists()) {
            helperDir.mkdir();
        }

        config.setBlocks();
    }

    private void createHelper(MageConfig config, File file) throws Exception {
        File helperDir = new File(file, "Helper");
        if (!helperDir.exists()) {
            helperDir.mkdir();
        }

        File dataHelper = new File(helperDir, "Data.php");
        magento.addHelper(config, dataHelper);
    }

    private MageConfig createEtc(File file) {
        File etcDir = new File(file, "etc");
        if (!etcDir.exists()) {
            etcDir.mkdir();
        }

        try {
            File configFile = new File(etcDir, "config.xml");
            return magento.createConfig(configFile, createNewExtensionDialog.getVersion());
        } catch (ParserConfigurationException e) {
        }
        return null;
    }

    @Override
    public void update(AnActionEvent e) {
        Project project = e.getProject();
        if (project == null) {
            return;
        }
//        ProjectFileIndex fileIndex = ProjectRootManager.getInstance(project).getFileIndex();

        // 選択されているファイルをリスト
        DataContext context = e.getDataContext();
        VirtualFile[] files = PlatformDataKeys.VIRTUAL_FILE_ARRAY.getData(context);
        if (files == null || files.length == 0) {
            return;
        }

        VirtualFile dir = files[0];
        if (dir.isDirectory() && dir.getName().equals(".modman")) {
            System.out.println(dir.getName());
            e.getPresentation().setEnabled(true);
        } else {
            e.getPresentation().setEnabled(false);
        }
    }
}
