package com.kodokux.mageidea.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.kodokux.mageidea.db.Model.Extension;
import com.kodokux.mageidea.db.MyConnection;
import com.kodokux.mageidea.service.Magento;
import com.kodokux.mageidea.xml.ConfigXmlUtil;

import java.io.File;
import java.sql.*;
import java.util.Collection;
import java.util.Iterator;


/**
 * User: johna1203
 * Date: 2013/09/15
 * Time: 23:00
 */
public class CacheConfigAction extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();

        Project project = (Project) PlatformDataKeys.PROJECT.getData(dataContext);
        if (project == null)
            return;

        Magento magento = Magento.getInstance(project);
        if (magento == null)
            return;

        MyConnection myConnection = MyConnection.getInstance(project);

        Collection<VirtualFile> psiFiles = FilenameIndex.getVirtualFilesByName(project, "config.xml", GlobalSearchScope.projectScope(project));

        Iterator<VirtualFile> iterator = psiFiles.iterator();

        try {
            myConnection.rebaseDB();
        } catch (SQLException e1) {
            e1.printStackTrace();
            return;
        }
        Extension extension = new Extension(myConnection.getConnection());

        while (iterator.hasNext()) {
            VirtualFile next = iterator.next();
            VirtualFile parent = next.getParent();
            if (parent != null && parent.getName().equals("etc")) {
                String extensionName = ConfigXmlUtil.getExtensionName(next);
                String extensionVersion = ConfigXmlUtil.getExtensionVersion(next);
                if (extensionName != null) {
                    String path = parent.getParent().getPath();
                    String channel = magento.getChannel(new File(next.getPath()));
                    String replace = path.replace(project.getBasePath() + "/", "");
                    extension.saveToConfig(next, channel, replace);
                }
            }
        }


        try {
            myConnection.show();
        } catch (SQLException e1) {
        }


//        try {
//            Class.forName("org.sqlite.JDBC");
//            String dbName = "mageidea.db";
//            String baseDir = project.getBasePath() + File.separator + ".idea";
//            String configPath = project.getBasePath() + File.separator + ".idea" + File.separator + "mage-idea";
//
//            File file = new File(configPath);
//            if (!file.exists()) {
//                if (!file.mkdirs()) {
//                    //ERROR;
//                } else {
//                    LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
//                }
//            }
//
//            String dbPath = configPath + File.separator + dbName;
//            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
//            Statement statement = connection.createStatement();
//            statement.setQueryTimeout(30);
//            LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(dbPath));
//            System.out.println(dbPath);
//
//            statement.executeUpdate("drop table if exists person");
//            statement.executeUpdate("create table person (id integer, name string)");
//            statement.executeUpdate("insert into person values(1, 'leo')");
//            statement.executeUpdate("insert into person values(2, 'yui')");
//            ResultSet rs = statement.executeQuery("select * from person");
//            while (rs.next()) {
//                // read the result set
//                System.out.println("name = " + rs.getString("name"));
//                System.out.println("id = " + rs.getInt("id"));
//            }
//
//        } catch (ClassNotFoundException e1) {
//            e1.printStackTrace();
//        } catch (SQLException e1) {
//            e1.printStackTrace();
//        }


    }


    public static void main(String[] args) throws ClassNotFoundException {
        // load the sqlite-JDBC driver using the current class loader
        Class.forName("org.sqlite.JDBC");

        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            statement.executeUpdate("drop table if exists person");
            statement.executeUpdate("create table person (id integer, name string)");
            statement.executeUpdate("insert into person values(1, 'leo')");
            statement.executeUpdate("insert into person values(2, 'yui')");
            ResultSet rs = statement.executeQuery("SELECT * FROM person");
            while (rs.next()) {
                // read the result set
                System.out.println("name = " + rs.getString("name"));
                System.out.println("id = " + rs.getInt("id"));
            }
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
    }
}
