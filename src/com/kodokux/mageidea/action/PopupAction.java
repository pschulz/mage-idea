package com.kodokux.mageidea.action;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.actionSystem.impl.ActionManagerImpl;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.ListPopup;
import com.magicento.actions.IMagicentoAction;
import com.magicento.actions.MagicentoActionAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * User: johna1203
 * Date: 2013/09/14
 * Time: 11:56
 */
public class PopupAction extends MagicentoActionAbstract implements IMagicentoAction {
    private AnActionEvent event;

//    @Override
//    public void actionPerformed(AnActionEvent anActionEvent) {
//
//
//    }

    @Override
    public void executeAction() {
        DefaultActionGroup actionGroup = new DefaultActionGroup();
        List<AnAction> actions = _getDefaultActions();

        for (AnAction action : actions) {
            actionGroup.add(action);
        }
        final ListPopup popup =
                JBPopupFactory.getInstance().createActionGroupPopup(
                        "Mage-idea Actions",
                        actionGroup,
                        getEvent().getDataContext(),
                        JBPopupFactory.ActionSelectionAid.SPEEDSEARCH,
                        false);

        popup.showInBestPositionFor(getEvent().getDataContext());
    }

    private List<AnAction> _getDefaultActions() {

        List<AnAction> actions = new ArrayList<AnAction>();

        String[] actionIds = {
                "ExtensionInfo",
//                "CacheConfigAction",
//                "OverrideThisClass",
//                "Magerun",

//                "AddTranslation",
//                "MagicentoTranslateText",
//                "GotoMagentoClass",
//                "AddFactoryToPhpStormMetaNamespace",
//                "AddVarPhpDoc",
//                "RemoveFromCoreResource",
//                "GoToBlockFromTemplate",
//                "GotoTemplateFromBlock",
//                "GoToBlockDefinitionInLayout",
//                "AddVarThisToTemplate",
                "CopyTemplate",
//                "GetStoreConfig",
//                "CompareWithOriginal",
                "RewriteClass",
                "RewriteController",
//                "EvaluateInMagento",
//                "GotoClassesOfFactory",
                "CreateModule",
                "CloneExtensionAction",
                "PackageExtensionAction"
//                "SetMagePath",
//                "SetStore",
//                "CreatePhpStormMetaNamespace",
//                "FlushCache",
//                "ToggleTemplateHints"

        };

        ActionManager actionManager = ActionManagerImpl.getInstance();
        for (String actionId : actionIds) {
            AnAction action = actionManager.getAction(actionId);

            if (action instanceof IMagicentoAction) {
                if (((IMagicentoAction) action).isApplicable(getEvent())) {
                    actions.add(action);
                }
            } else {
                if (action != null)
                    actions.add(action);
            }
        }
        return actions;


    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
    }


    @Override
    public Boolean isApplicable(AnActionEvent e) {
        return true;
    }
}
