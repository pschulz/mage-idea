package com.kodokux.mageidea.action;

import com.intellij.codeInsight.TargetElementUtilBase;
import com.intellij.ide.util.gotoByName.GotoClassModel2;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.pom.Navigatable;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiUtilCore;
import com.kodokux.mageidea.Icons;
import com.kodokux.mageidea.jetty.HelloWorld;
import com.kodokux.mageidea.jetty.MessageHandlerInterface;
import com.kodokux.mageidea.jetty.MyWebSocket;
import com.kodokux.mageidea.jetty.WebSocketServer;
import org.eclipse.jetty.server.Server;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/18
 * Time: 14:53
 */
public class JettyServerAction extends AnAction implements MessageHandlerInterface {
    private Project project;
    static WebSocketServer webSocketServer = null;


    public JettyServerAction() {
        super("Start server", "Kodoku-debug server", Icons.MAGENTO_ICON_16);
    }

    @Override
    public void actionPerformed(AnActionEvent e) {

        project = e.getData(PlatformDataKeys.PROJECT);

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (webSocketServer == null) {
                    webSocketServer = new WebSocketServer(JettyServerAction.this);
                    webSocketServer.setHost("localhost");
                    webSocketServer.setPort(12032);
                    //        webSocketServer.setKeyStoreResource(new FileResource(WebSocketServer.class.getResource("/keystore.jks")));
                    //        webSocketServer.setKeyStorePassword("password");
                    //        webSocketServer.setKeyManagerPassword("password");
                    webSocketServer.addWebSocket(MyWebSocket.class, "/");
                    webSocketServer.initialize();
                    try {
                        webSocketServer.start();
                    } catch (Exception e1) {
                    }
//                    MagentoComponent.showInfoNotification(project, "Start server");
                } else {
                    try {
                        webSocketServer.stop();
                        webSocketServer = null;
//                        MagentoComponent.showInfoNotification(project, "Stop server");
                    } catch (Exception e1) {
                    }
                }
            }
        }).start();


    }

    @Override
    public void update(AnActionEvent e) {
        Project project = (Project) PlatformDataKeys.PROJECT.getData(e.getDataContext());
        Presentation presentation = e.getPresentation();
        presentation.setVisible(true);
        if (project == null)
            presentation.setVisible(false);
        else if ("MainToolbar".equals(e.getPlace())) {
            if (webSocketServer == null) {
                presentation.setIcon(Icons.MAGENTO_ICON_16);
            } else {
                presentation.setIcon(Icons.MAGENTO_ICON_16_ON);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(12032);
        server.setHandler(new HelloWorld());
        server.start();
        server.join();
    }

    @Override
    public void onMessage(final String message) {
        if (project != null) {
            System.out.println(message);
            if (message.startsWith("open:")) {
                String filePath = message.substring("open:".length());
                final String trim = filePath.trim();
                if (!trim.startsWith("/")) {
                    ApplicationManager.getApplication().invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            String file = project.getBaseDir().getUrl() + "/" + trim;
                            final VirtualFile virtualFile = VirtualFileManager.getInstance().findFileByUrl(file);
                            if (virtualFile != null) {
                                FileEditorManager.getInstance(project).openFile(virtualFile, true);
                            }
                        }
                    });
                }
            } else if (message.startsWith("class:")) {
                ApplicationManager.getApplication().invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        String className = message.substring("class:".length()).trim();
                        GotoClassModel2 model = new GotoClassModel2(project);
                        Object[] elements = model.getElementsByName(className, true, className);
                        System.out.println(elements.length);
                        if (elements.length > 0) {
                            PsiElement psiElement = (PsiElement) elements[0];
                            PsiElement navElement = psiElement.getNavigationElement();
                            navElement = TargetElementUtilBase.getInstance().getGotoDeclarationTarget(psiElement, navElement);
                            if (navElement instanceof Navigatable) {
                                if (((Navigatable) navElement).canNavigate()) {
                                    ((Navigatable) navElement).navigate(true);
                                }
                            } else if (navElement != null) {
                                int navOffset = navElement.getTextOffset();
                                VirtualFile virtualFile = PsiUtilCore.getVirtualFile(navElement);
                                if (virtualFile != null) {
                                    new OpenFileDescriptor(project, virtualFile, navOffset).navigate(true);
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
