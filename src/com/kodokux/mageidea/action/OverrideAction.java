package com.kodokux.mageidea.action;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.jetbrains.php.lang.psi.PhpFile;
import com.kodokux.mageidea.action.ui.OverrideDialog;
import com.kodokux.mageidea.service.Magento;
import com.kodokux.mageidea.templates.MagentoFileFromTemplateDataProvider;

import java.io.File;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 17:45
 */
public class OverrideAction extends NewBaseAction {


    public OverrideAction() {
        super("Override this class", "Override this class");
    }


    protected MagentoFileFromTemplateDataProvider getDataProvider(Project project, PsiDirectory dir, Editor editor, PsiFile file) {
        OverrideDialog dialog = new OverrideDialog(project, dir, editor, file);
        dialog.show();

        if (!dialog.isOK())
            return null;
        else
            return dialog;
    }

    @Override
    public void update(AnActionEvent e) {

        VirtualFile file = e.getData(PlatformDataKeys.VIRTUAL_FILE);
        if (file == null || file instanceof PhpFile) {
            e.getPresentation().setEnabled(false);
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(e.getDataContext());
        if (project == null) {
            e.getPresentation().setEnabled(false);
            return;
        }

        Magento magento = Magento.getInstance(project);
        String fileType = magento.getFileType(new File(file.getPath()));
        if (!fileType.equals("Block") && !fileType.equals("Model") && !fileType.equals("Helper")) {
            e.getPresentation().setEnabled(false);
            return;
        }


        e.getPresentation().setEnabled(true);
    }

    //    public void actionPerformed(AnActionEvent e) {
//        Project project = e.getData(PlatformDataKeys.PROJECT);
//
//        final VirtualFile file = e.getData(PlatformDataKeys.VIRTUAL_FILE);
//        PsiFile psiFile = e.getData(LangDataKeys.PSI_FILE);
//        Editor editor = e.getData(PlatformDataKeys.EDITOR);
//
//        if (file == null || psiFile == null || editor == null)
//            return;
//
//
//        Magento magento = Magento.getInstance(project);
//
//
//        File currentFile = new File(file.getPath());
//        if (currentFile.isFile() && file.getFileType() == PhpFileType.INSTANCE) {
//            String fileType = magento.getFileType(currentFile);
//            if (fileType != null) {
//                String dialogLabel = magento.getExtensionName(currentFile) + " / " + fileType + " / " + magento.getClassName(currentFile);
//
////                int offset = editor.getCaretModel().getOffset();
////                PsiElement elementAt = psiFile.findElementAt(offset);
////                Method tests = PsiTreeUtil.getParentOfType(elementAt, Method.class);
//
////                System.out.println(tests.getName());
//
//
//                OverrideDialog overrideDialog = new OverrideDialog(project, dialogLabel);
//                overrideDialog.setTitle(file.getPath());
//                List<Extension> extensionList = magento.getExtensionList();
//                overrideDialog.setExtensionList(extensionList);
//                overrideDialog.show();
//                if (overrideDialog.isOK()) {
//
//                }
//
//
////                ;
//
////                Collection<Method> tests = PsiTreeUtil.findChildrenOfType(psiFile.getFirstChild(), Method.class);
////
////                System.out.println("METHODs count : " + tests.size());
////                for (Method methodReference : tests) {
////                    System.out.println("METHODS : " + methodReference.getName());
////                }
////
////
////                if (overrideDialog.isOK()) {
////
////
////
////
////                }
//            }
//        }
//    }
}
