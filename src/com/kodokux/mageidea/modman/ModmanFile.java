package com.kodokux.mageidea.modman;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import io.netty.util.internal.StringUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/09
 * Time: 19:47
 */
public class ModmanFile {
    private final Project myProject;
    private final File rootFile;
    private final File modmanFile;
    private List<String> links = new ArrayList<String>();

    public ModmanFile(Project project, File file, File modman) {
        myProject = project;
        rootFile = file;
        modmanFile = modman;


        init();
    }

    private void init() {
        VirtualFile fileByIoFile = LocalFileSystem.getInstance().findFileByIoFile(rootFile);

        VirtualFile modmanVirtualFile = LocalFileSystem.getInstance().findFileByIoFile(modmanFile);

        if (modmanVirtualFile != null) {
            try {
                FileReader f = new FileReader(modmanVirtualFile.getPath());
                BufferedReader b = new BufferedReader(f);
                String s;
                while ((s = b.readLine()) != null) {
                    String[] split = StringUtil.split(s, '\t');
                    if (split.length == 2) {
                        File checkFile = new File(rootFile, split[0]);
                        VirtualFile v = LocalFileSystem.getInstance().findFileByIoFile(checkFile);
                        if (checkFile.exists() && canCreateLink(v))
                            addLink(split[0]);
                    }
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
        }


        findNotExistFolderInMagento(fileByIoFile);
    }

    private void findNotExistFolderInMagento(VirtualFile fileByIoFile) {
        if (fileByIoFile != null) {
            VirtualFile[] childrens = fileByIoFile.getChildren();
            for (VirtualFile children : childrens) {

                if (children.getName().equals(".git")
                        || children.getName().equals("modman")) {
                    continue;
                }


                String relativePath = FileUtil.getRelativePath(rootFile.getPath(), children.getPath(), '/');

                if (!canCreateLink(children)) {
                    findNotExistFolderInMagento(children);
                } else {
                    addLink(relativePath);
                }
            }
        }
    }

    private boolean canCreateLink(VirtualFile virtualFile) {

        String relativePath = FileUtil.getRelativePath(rootFile.getPath(), virtualFile.getPath(), '/');

        File magentoFile = new File(myProject.getBasePath(), relativePath);

        if (!magentoFile.exists()) {
            if (hasFolder(virtualFile, "code")) {
                VirtualFile parent = virtualFile.getParent();
                String parentName = parent.getName();
                if (parentName.equals("community") || parentName.equals("local") || parent.getName().equals("core")) {
                    if (virtualFile.isDirectory())
                        createFolder(magentoFile.getPath());
                    return false;
                } else {
                    VirtualFile configXml = virtualFile.findFileByRelativePath("etc/config.xml");
                    if (configXml != null) {
                        return true;
                    }
                }
            }

            if (!virtualFile.isDirectory()) {
                return true;
            } else {
                createFolder(magentoFile.getPath());
            }
        } else {
            try {
                String canonicalPath = magentoFile.getCanonicalPath();
                if (canonicalPath.equals(virtualFile.getPath())) {
                    return true;
                }
            } catch (IOException e) {
            }
        }
        return false;
    }

    protected void createFolder(String folder) {
        try {
            VfsUtil.createDirectories(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean hasFolder(VirtualFile virtualFile, String code) {
        boolean flg = false;

        if (virtualFile != null && virtualFile.exists()) {

            if (rootFile.getPath().equals(virtualFile.getPath())) {
                return false;
            }

            VirtualFile parent = virtualFile.getParent();
            if (parent != null && parent.getName().equals(code)) {
                return true;
            } else {
                flg = hasFolder(parent, code);
            }
        }
        return flg;
    }

    private void addLink(String path) {
        boolean addLinkFlg = true;
        for (String link : links) {
            if (path.startsWith(link)) {
                addLinkFlg = false;
            }
        }

        if (addLinkFlg) {
            links.add(path);
        }

    }

    public List<String> getLinks() {
        return links;
    }
}
