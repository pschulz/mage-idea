package com.kodokux.mageidea.modman;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.EditorComboBox;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 1:16
 */
public class CloneDialog extends DialogWrapper {
    private final Project myProject;
    private JPanel mainContent;
    private EditorComboBox gitUrlComboBox;

    protected CloneDialog(@NotNull Project project) {
        super(project);

        myProject = project;

        setTitle("Clone Project");

        mainContent.setPreferredSize(new Dimension(500, -1));
        init();
    }

    @Nullable
    @Override
    public JComponent getPreferredFocusedComponent() {
        return gitUrlComboBox;
    }

    //    @Override
//    protected void doOKAction() {
//        System.out.println(gitUrlTextField.getText());
//
//        Settings settings = Settings.getInstance();
//        String modmanPath = settings.modmanPath;
//
//        ModmanCommandExecutor commandExecutor = new ModmanCommandExecutor(myProject, modmanPath) {
//            @Override
//            protected void provideFeedback(boolean flag, String s) {
//                infoLabel.setText(s);
//            }
//
//            @Override
//            protected String getProgressTitle() {
//                return "Modman clone";
//            }
//
//            @Override
//            protected String[] getCommand(Project project, String modmanPath) {
//
//                String[] path = new String[]{
//                        "clone",
//                        gitUrlTextField.getText()
//                };
//
//                return path;
//            }
//        };
//
//        commandExecutor.execute();
//
//
//        super.doOKAction();
//    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return mainContent;
    }

    public String getSourceRepositoryURL() {
        return gitUrlComboBox.getText();
    }

    private void createUIComponents() {
        gitUrlComboBox = new EditorComboBox("");
    }
}
