package com.kodokux.mageidea.modman;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.process.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.LocalFileSystem;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * User: johna1203
 * Date: 2013/10/08
 * Time: 0:37
 */
public abstract class ModmanCommandExecutor {
    private final Project myProject;
    private final String myModmanPath;

    protected ModmanCommandExecutor(Project project, String modmanPath) {
        myProject = project;
        myModmanPath = modmanPath;
    }

    public void execute() {
        Task task = new Task.Modal(myProject, getProgressTitle(), true) {
            @Override
            public void run(@NotNull final ProgressIndicator indicator) {
                final StringBuilder outputBuilder = new StringBuilder();
                indicator.setIndeterminate(true);
                String[] myCommand = getCommand(myProject, myModmanPath);

                final StringBuilder sb = new StringBuilder();
                sb.append("Running");
                indicator.setText(sb.toString());
                boolean cancelledByUser = false;
                ExecutionException exception = null;

                try {
                    OSProcessHandler processHandler = ScriptRunnerUtil.execute(myModmanPath, myProject.getBasePath(), null, myCommand);
                    processHandler.addProcessListener(new ProcessAdapter() {
                        @Override
                        public void onTextAvailable(ProcessEvent event, Key outputType) {
//                            System.out.println("onTextAvailable : " + event.getText());
                            outputBuilder.append(event.getText());
                            indicator.setText(outputBuilder.toString());
                        }

                        @Override
                        public void processTerminated(ProcessEvent event) {
                            File file = new File(myProject.getBasePath() + File.pathSeparator + ".modman");
                            if (file.exists()) {
                                LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
                            }
                        }
                    });

                    processHandler.startNotify();
                    do {
                        boolean finished = processHandler.waitFor(1000L);
                        if (finished)
                            break;
                    } while (!indicator.isCanceled());

                    if (indicator.isCanceled()) {
                        cancelledByUser = true;
                        OSProcessManager.getInstance().killProcessTree(processHandler.getProcess());
                    }

                } catch (ExecutionException e) {
                    exception = e;
                }

                final String output = outputBuilder.toString();
                final boolean success = exception == null && !cancelledByUser;

                ApplicationManager.getApplication().invokeLater(new Runnable() {
                    public void run() {
                        provideFeedback(success, output);
                    }
                });
            }
        };

        ProgressManager.getInstance().run(task);
    }

    protected abstract void provideFeedback(boolean flag, String s);

    protected abstract String getProgressTitle();

    protected abstract String[] getCommand(Project project, String modmanPath);
}
