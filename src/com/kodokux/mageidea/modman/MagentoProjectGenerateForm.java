package com.kodokux.mageidea.modman;

import com.intellij.platform.WebProjectGenerator;
import com.intellij.util.containers.ContainerUtilRt;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 2:40
 */
public class MagentoProjectGenerateForm {
    private JTextField magentoTextField;
    private JPanel mainPanel;
    private final List myStateListeners = ContainerUtilRt.newArrayList();

    public MagentoProjectGenerateForm() {
        magentoTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                fireStateChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    public JComponent getMainContenPanel() {
        return mainPanel;
    }

    public void addSettingsStateListener(WebProjectGenerator.SettingsStateListener listener) {
        myStateListeners.add(listener);
    }

    private void fireStateChanged() {
        WebProjectGenerator.SettingsStateListener listener;
        for (Iterator i$ = myStateListeners.iterator(); i$.hasNext(); listener.stateChanged(true))
            listener = (WebProjectGenerator.SettingsStateListener) i$.next();

    }
}
