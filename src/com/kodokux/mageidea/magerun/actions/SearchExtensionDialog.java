package com.kodokux.mageidea.magerun.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.HoverHyperlinkLabel;
import com.intellij.ui.components.JBList;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/01
 * Time: 18:55
 */
public class SearchExtensionDialog extends DialogWrapper {

    private final Project myProject;
    private final VirtualFile myWorkingDir;
    private HoverHyperlinkLabel myInstallationFeedbackLinkLabel;
    private Runnable myChangesListener;
    private JPanel panel1;
    private JTextField searchPackage;
    private JBList extensionNameList;
    private JTextArea packageInfo;


    public SearchExtensionDialog(@Nullable Project project, VirtualFile workingDir) {
        super(project);
        myProject = project;
        myWorkingDir = workingDir;
        setTitle("Search Extension");
        setUpUI();

        myInstallationFeedbackLinkLabel.setVisible(false);


        init();
        pack();
    }

    private void setUpUI() {
        createUIComponents();
    }

    private void createUIComponents() {
        myInstallationFeedbackLinkLabel = new HoverHyperlinkLabel("");
        setOKActionEnabled(false);
        setOKButtonText("Search");
        setOKButtonMnemonic(73);
        setCancelButtonText("Cancel");

        final DefaultListModel model = new DefaultListModel();
        myChangesListener = new Runnable() {
            public void run() {
                String selectedValue = (String) extensionNameList.getSelectedValue();
                packageInfo.setText(selectedValue);
            }
        };

        extensionNameList.setModel(model);

        extensionNameList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                myChangesListener.run();
            }
        });


        final Runnable myChangePackageName = new Runnable() {
            @Override
            public void run() {
                model.addElement("johna");
            }
        };

        searchPackage.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                System.out.println("insertUpdate");
                myChangePackageName.run();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                System.out.println("removeUpdate");
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                System.out.println("changedUpdate");

            }

//            public void warn() {
//                if (Integer.parseInt(textField.getText()) <= 0) {
//                    JOptionPane.showMessageDialog(null,
//                            "Error: Please enter number bigger than 0", "Error Massage",
//                            JOptionPane.ERROR_MESSAGE);
//                }
//            }
        });


    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return panel1;
    }
}
