package com.kodokux.mageidea.magerun;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.process.*;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.jetbrains.php.config.PhpCommandLineCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/01
 * Time: 18:10
 */
public abstract class MagerunCommandExecutor {

    private final Project myProject;
    private final String myMagerunPath;

    public MagerunCommandExecutor(Project project, String magerunPath) throws ExecutionException {
        myProject = project;
        myMagerunPath = magerunPath;

        PhpCommandLineCommand phpCommandLine = PhpCommandLineCommand.create(project);
    }


    public void execute() {
        AtomicReference<Task> task = new AtomicReference<Task>(new Task.Modal(myProject, getProgressTitle(), true) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {

                final StringBuilder outputBuilder = new StringBuilder();
                ExecutionException exception = null;
                boolean cancelledByUser = false;

                indicator.setIndeterminate(true);
                String myCommand[] = getCommand(myProject, myMagerunPath);
                StringBuilder sb = new StringBuilder();

                sb.append(" ").append("johna then");

                indicator.setText(sb.toString());


                try {
                    OSProcessHandler processHandler = ScriptRunnerUtil.execute(myCommand[0], myProject.getBasePath(), null, (String[]) Arrays.copyOfRange(myCommand, 1, myCommand.length));

                    processHandler.addProcessListener(new ProcessAdapter() {
                        @Override
                        public void onTextAvailable(ProcessEvent event, Key outputType) {
                            outputBuilder.append(event.getText());
                        }
                    });

                    processHandler.startNotify();


                    do {
                        boolean finished = processHandler.waitFor(1000L);
                        if (finished)
                            break;
                    } while (!indicator.isCanceled());


                    cancelledByUser = true;
                    OSProcessManager.getInstance().killProcessTree(processHandler.getProcess());

                } catch (ExecutionException e) {
                    exception = e;
                }

                String output = outputBuilder.toString();

                System.out.println(output);

                final boolean success = exception == null && !cancelledByUser && output != null;
            }
        });
    }

    protected abstract String[] getCommand(Project project, String magerunPath);

    protected abstract String getProgressTitle();
}

