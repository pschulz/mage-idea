package com.kodokux.mageidea.db.Model;

import java.sql.*;

/**
 * User: johna1203
 * Date: 2013/09/22
 * Time: 23:44
 */
public class Rewrite extends AbstractModel {

    static public String TABLE_NAME = "extension_rewrite";

    public Rewrite(Connection connection) {
        super(connection);
    }


    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    public int insertOrUpdate(String extensionId,
                              String rewriteType,
                              String tagName,
                              String rewriteClassShortName,
                              String rewriteClass) {
        try {
            PreparedStatement preparedStatement =
                    getConnection().prepareStatement(
                            "SELECT * FROM " + getTableName() + " WHERE extensionId = ? AND rewriteType = ? AND tagName = ? AND rewriteClassShortName = ?");
            getConnection().setAutoCommit(false);
            preparedStatement.setString(1, extensionId);
            preparedStatement.setString(2, rewriteType);
            preparedStatement.setString(3, tagName);
            preparedStatement.setString(4, rewriteClassShortName);
            ResultSet result = preparedStatement.executeQuery();

            int id = 0;
            if (result.next()) {
                id = result.getInt(1);
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("UPDATE " + getTableName() + " SET " +
                                "extensionId = ?, " +
                                "rewriteType = ?, " +
                                "tagName = ?, " +
                                "rewriteClassShortName = ?," +
                                "rewriteClass = ?" +
                                "where id = ?");
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, rewriteType);
                insertStatement.setString(3, tagName);
                insertStatement.setString(4, rewriteClassShortName);
                insertStatement.setString(5, rewriteClass);
                insertStatement.setInt(6, id);
                insertStatement.executeUpdate();
            } else {
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("INSERT INTO  " + getTableName() + " values(" +
                                "null, ?, ?, ?, ?, ?" +
                                ")", Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, rewriteType);
                insertStatement.setString(3, tagName);
                insertStatement.setString(4, rewriteClassShortName);
                insertStatement.setString(5, rewriteClass);
                insertStatement.executeUpdate();
                ResultSet generatedKeys = insertStatement.getGeneratedKeys();
                generatedKeys.next();
                id = generatedKeys.getInt(1);
            }
            getConnection().commit();

            return id;

        } catch (SQLException e) {
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
            }
            e.printStackTrace();
            return 0;
        }

    }
}
