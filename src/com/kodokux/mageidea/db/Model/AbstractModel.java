package com.kodokux.mageidea.db.Model;

import com.intellij.openapi.vfs.VirtualFile;

import java.sql.Connection;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/22
 * Time: 19:40
 */
abstract public class AbstractModel {

    private Connection connection;

    public AbstractModel(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    abstract String getTableName();

}
