package com.kodokux.mageidea.db.Model;

import com.intellij.openapi.vfs.VirtualFile;
import com.kodokux.mageidea.xml.ConfigXmlUtil;

import java.sql.*;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/22
 * Time: 19:40
 */
public class Extension extends AbstractModel {

    static public String TABLE_NAME = "extensions";

    public Extension(Connection connection) {
        super(connection);
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    public int insertOrSave(String extensionName,
                            String version,
                            String channel,
                            String fullPath) {
        try {
            PreparedStatement preparedStatement =
                    getConnection().prepareStatement(
                            "SELECT * FROM " + getTableName() + " WHERE extensionName = ?");
            getConnection().setAutoCommit(false);
            preparedStatement.setString(1, extensionName);
            ResultSet result = preparedStatement.executeQuery();

            int id = 0;
            if (result.next()) {
                id = result.getInt(1);
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("UPDATE " + getTableName() + " SET " +
                                "extensionName = ?, " +
                                "version = ?, " +
                                "channel = ?, " +
                                "fullPath = ?" +
                                "where id = ?");
                insertStatement.setString(1, extensionName);
                insertStatement.setString(2, version);
                insertStatement.setString(3, channel);
                insertStatement.setString(4, fullPath);
                insertStatement.setInt(5, id);
                insertStatement.executeUpdate();
            } else {
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("INSERT INTO  " + getTableName() + " values(" +
                                "null , ?, ?, ?, ?" +
                                ")", Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, extensionName);
                insertStatement.setString(2, version);
                insertStatement.setString(3, channel);
                insertStatement.setString(4, fullPath);
                insertStatement.executeUpdate();
                ResultSet generatedKeys = insertStatement.getGeneratedKeys();
                generatedKeys.next();
                id = generatedKeys.getInt(1);
            }
            getConnection().commit();

            return id;

        } catch (SQLException e) {
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
            }
            e.printStackTrace();
            return 0;
        }
    }

    public void saveToConfig(VirtualFile virtualFile, String channel, String rootPath) {
        String extensionName = ConfigXmlUtil.getExtensionName(virtualFile);
        String extensionVersion = ConfigXmlUtil.getExtensionVersion(virtualFile);
        int extensionId = insertOrSave(extensionName,
                extensionVersion,
                channel,
                rootPath);
        //events
        ConfigXmlUtil.Event[] events = ConfigXmlUtil.getEvents(virtualFile);

        Event event = new Event(getConnection());
        for (int i = 0; i < events.length; i++) {
            event.insertOrUpdate(
                    Integer.toString(extensionId),
                    events[i].getEventName(),
                    events[i].getEventType(),
                    events[i].getClassName(),
                    events[i].getMethod()
            );
        }

        Rewrite rewrite = new Rewrite(getConnection());
        ConfigXmlUtil.Rewrite[] rewrites = ConfigXmlUtil.getRewrites(virtualFile);

        for (int i = 0; i < rewrites.length; i++) {
            rewrite.insertOrUpdate(
                    Integer.toString(extensionId),
                    rewrites[i].getRewriteType(),
                    rewrites[i].getTagName(),
                    rewrites[i].getRewriteClassShortName(),
                    rewrites[i].getRewriteClass()
            );
        }


        OverloadController overloadController1 = new OverloadController(getConnection());
        ConfigXmlUtil.OverloadController[] overloadController = ConfigXmlUtil.getOverloadController(virtualFile);
        for (int i = 0; i < overloadController.length; i++) {
            overloadController1.insertOrSave(
                    Integer.toString(extensionId),
                    overloadController[i].getType(),
                    overloadController[i].getRootName(),
                    overloadController[i].getNameSpace(),
                    overloadController[i].getBefore(),
                    overloadController[i].getClassName()
            );
        }

//        try {
//            getConnection().close();
//        } catch (SQLException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }

    }


}
