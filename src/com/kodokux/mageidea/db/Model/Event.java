package com.kodokux.mageidea.db.Model;

import java.sql.*;

/**
 * User: johna1203
 * Date: 2013/09/22
 * Time: 23:44
 */
public class Event extends AbstractModel {

    static public String TABLE_NAME = "extension_events";

    public Event(Connection connection) {
        super(connection);
    }


    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    public int insertOrUpdate(String extensionId,
                              String eventName,
                              String eventType,
                              String className,
                              String methodName) {
        try {
            PreparedStatement preparedStatement =
                    getConnection().prepareStatement(
                            "SELECT * FROM " + getTableName() + " WHERE extensionId = ? AND eventName = ? AND eventType = ?");
            getConnection().setAutoCommit(false);
            preparedStatement.setString(1, extensionId);
            preparedStatement.setString(2, eventName);
            preparedStatement.setString(3, eventType);
            ResultSet result = preparedStatement.executeQuery();

            int id = 0;
            if (result.next()) {
                id = result.getInt(1);
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("UPDATE " + getTableName() + " SET " +
                                "extensionId = ?, " +
                                "eventName = ?, " +
                                "eventType = ?, " +
                                "className = ?," +
                                "methodName = ?" +
                                "where id = ?");
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, eventName);
                insertStatement.setString(3, eventType);
                insertStatement.setString(4, className);
                insertStatement.setString(5, methodName);
                insertStatement.setInt(6, id);
                insertStatement.executeUpdate();
            } else {
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("INSERT INTO  " + getTableName() + " values(" +
                                "null, ?, ?, ?, ?, ?" +
                                ")", Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, eventName);
                insertStatement.setString(3, eventType);
                insertStatement.setString(4, className);
                insertStatement.setString(5, methodName);
                insertStatement.executeUpdate();
                ResultSet generatedKeys = insertStatement.getGeneratedKeys();
                generatedKeys.next();
                id = generatedKeys.getInt(1);
            }
            getConnection().commit();

            return id;

        } catch (SQLException e) {
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
            }
            e.printStackTrace();
            return 0;
        }

    }
}
