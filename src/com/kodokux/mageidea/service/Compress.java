package com.kodokux.mageidea.service;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/10
 * Time: 17:53
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class defines two static methods for gzipping files and zipping
 * directories. It also defines a demonstration program as a nested class.
 */
public class Compress {
    public static void main(String[] args) throws Exception {

        String sourceFolderName = "C:\\JustExampleFolder";
        String outputFileName = "C:\\JustExample.zip";

        FileOutputStream fos = new FileOutputStream(outputFileName);
        ZipOutputStream zos = new ZipOutputStream(fos);
        //level - the compression level (0-9)
        zos.setLevel(9);

        System.out.println("Begin to compress folder : " + sourceFolderName + " to " + outputFileName);
        addFolder(zos, sourceFolderName, sourceFolderName);

        zos.close();
        System.out.println("Program ended successfully!");
    }

    public static void addFolder(ZipOutputStream zos, String folderName, String baseFolderName) throws Exception {
        File f = new File(folderName);
        if (f.exists()) {
            if (f.isDirectory()) {
                File f2[] = f.listFiles();
                for (int i = 0; i < f2.length; i++) {
                    addFolder(zos, f2[i].getAbsolutePath(), baseFolderName);
                }
            } else {
                //add file
                //extract the relative name for entry purpose
                String entryName = folderName.substring(baseFolderName.length() + 1, folderName.length());
                System.out.print("Adding entry " + entryName + "...");
                ZipEntry ze = new ZipEntry(entryName);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(folderName);
                int len;
                byte buffer[] = new byte[1024];
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
                zos.closeEntry();
                System.out.println("OK!");

            }
        } else {
            System.out.println("File or directory not found " + folderName);
        }

    }
}