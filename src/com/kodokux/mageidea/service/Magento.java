package com.kodokux.mageidea.service;

import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.ide.fileTemplates.FileTemplateUtil;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopes;
import com.kodokux.mageidea.module.Extension;
import com.kodokux.mageidea.xml.ConfigXmlUtil;
import com.kodokux.mageidea.xml.MageConfig;
import io.netty.util.internal.StringUtil;
import org.jetbrains.annotations.NotNull;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.util.*;

/**
 * User: johna1203
 * Date: 2013/09/14
 * Time: 12:10
 */
public class Magento {

    static Magento INSTANCE;
    final String FILE_TYPE_BLOCK = "Block";
    final String[] EXTENSION_BASE_PATH = {
            "app/code/core",
            "app/code/community",
            "app/code/local",
    };

    private final Project _project;

    public static Magento getInstance(Project project) {
        return ServiceManager.getService(project, Magento.class);
    }

    public Magento(@NotNull Project project) {
        this._project = project;
    }

    public boolean isRoot(File file, boolean checkModulePath) {

        if (checkModulePath) {
            return isModulePath(file);
        }
        return file.getPath().equals(getProjectRootPath());
    }

    public boolean isModulePath(File file) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            File parent = file;
            if (parent != null) {
                String s = parent.getName().toLowerCase();
                if (i != 0)
                    s = s + "/";
                stringBuilder.insert(0, s);
                file = parent.getParentFile();
            }
        }

        return Arrays.asList(EXTENSION_BASE_PATH).contains(stringBuilder.toString());
    }

    public String getProjectRootPath() {
        return _project.getBasePath();
    }

    public String getFileType(File file) {
        String extensionName = getExtensionName(file);

        String[] strings = extensionName.split("_");

        if (strings.length > 0) {
            String vendorDirName = strings[0];
            String moduleDirName = strings[1];

            while (!isRoot(file, true)) {
                String name = file.getName();

                if (name.equals("controllers")
                        || name.equals("Block")
                        || name.equals("etc")
                        || name.equals("Helper")
                        || name.equals("Model")
                        || name.equals("sql")
                        ) {
                    File moduleFile = file.getParentFile();
                    if (moduleFile != null && moduleFile.getName().equals(moduleDirName)) {
                        File vendorFile = moduleFile.getParentFile();
                        if (vendorFile != null && vendorFile.getName().equals(vendorDirName)) {
                            return name;
                        }
                    }
                }
                file = file.getParentFile();
            }
        }
        return null;
    }

    protected List<String> getListPath(File file) {
        List<String> list = new ArrayList<String>();
        while (!isRoot(file, true)) {
            if (file == null)
                break;
            list.add(file.getName());
            file = file.getParentFile();
        }
        Collections.reverse(list);
        return list;
    }

    public String getExtensionName(File file) {
        String moduleName = "";
        String oldName = "";
        while (!isRoot(file, true)) {
            if (file == null)
                break;
            moduleName = file.getName() + "_" + oldName;
            oldName = file.getName();
            file = file.getParentFile();
        }
        return moduleName;
    }

    public String getExtensionName(VirtualFile virtualFile) {
        String path = virtualFile.getPath();
        System.out.println(path);
        return getExtensionName(new File(path));
    }

    public String getChannel(File file) {
        while (!isRoot(file, true)) {
            if (file == null)
                break;
            file = file.getParentFile();
        }
        if (file != null) {
            return file.getName();
        }
        return null;
    }

    public String getModuleName(File file) {
        String extensionName1 = getExtensionName(file);
        String[] split = extensionName1.split("_");

        String moduleName = "";
        if (split.length > 0) {
            moduleName = split[1].toLowerCase();
        }
        return moduleName;
    }

    public String getClassName(File file) {
        List<String> listPath = getListPath(file);
        StringBuilder stringBuilder = new StringBuilder();
        for (String name : listPath) {
            name = name.substring(0, 1).toUpperCase() + name.substring(1);
            stringBuilder.append(name);
            stringBuilder.append("_");
        }

        String className = stringBuilder.toString();
        if (className.length() > 0) {
            className = className.substring(0, className.length() - 1);
        }

        if (className.lastIndexOf(".php") == className.length() - 4) {
            className = className.substring(0, className.length() - 4);

            if (getFileType(file).equals("controllers")) {
                className = className.replace("_Controllers_", "_");
            }

            return className;
        }

        return "";
    }

    public MageConfig createConfig(File file, String version) throws ParserConfigurationException {
        MageConfig config = new MageConfig();
        String extensionName = getExtensionName(file);
        config.setExtensionName(extensionName);
//        config.setModels();
        config.setVersion(extensionName, version);
        config.save(file, _project);
        return config;
    }

    public void addHelper(MageConfig config, File file) throws Exception {
//        if (file.isFile()) {
        FileTemplateManager fileTemplateManager = FileTemplateManager.getInstance();
        Properties properties = fileTemplateManager.getDefaultProperties();
        properties.setProperty("CLASSNAME", getClassName(file));

        System.out.println("addHelper");
        VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file.getParentFile());
        if (virtualFile != null) {
            System.out.println("createFileFromTemplate after");
            PsiDirectory directory = PsiManager.getInstance(_project).findDirectory(virtualFile);
            createFileFromTemplate(directory, "base_helper.php", "Data", properties);
        } else {
            System.out.println("virtualFile != null");
        }
        config.setHelpers();
    }

    public void addBlocks(MageConfig config, File file) throws Exception {
        FileTemplateManager fileTemplateManager = FileTemplateManager.getInstance();
        Properties properties = fileTemplateManager.getDefaultProperties();
        properties.setProperty("CLASSNAME", getClassName(file));

        VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file.getParentFile());
        if (virtualFile != null) {
            System.out.println("createFileFromTemplate after");
            PsiDirectory directory = PsiManager.getInstance(_project).findDirectory(virtualFile);
            createFileFromTemplate(directory, "base_helper.php", "Data", properties);
        } else {
            System.out.println("virtualFile != null");
        }
        config.setHelpers();
    }

    public void createFileFromTemplate(PsiDirectory directory, String templateName, String fileName, Properties properties) throws Exception {
        final FileTemplate template =
                FileTemplateManager.getInstance().getInternalTemplate(templateName);
        FileTemplateUtil.createFromTemplate(template, fileName, properties, directory);
    }

    public List<Extension> getExtensionList() {
        Collection<VirtualFile> psiFiles = FilenameIndex.getVirtualFilesByName(_project, ".modman", GlobalSearchScope.projectScope(_project));
        VirtualFile projectRoot = _project.getBaseDir();

        List<Extension> extensionArrayList = new ArrayList<Extension>();
        for (VirtualFile virtualFile : psiFiles) {
            VirtualFile parent = virtualFile.getParent();
            if (projectRoot.equals(parent)) {
                Collection<VirtualFile> configVirtualFiles = FilenameIndex.getVirtualFilesByName(_project, "config.xml", GlobalSearchScopes.directoryScope(_project, virtualFile, true));
                for (VirtualFile configVirtualFile : configVirtualFiles) {
                    if (configVirtualFile.getParent() != null && configVirtualFile.getParent().getName().equals("etc")) {
                        String extensionName = ConfigXmlUtil.getExtensionName(configVirtualFile);
                        if (extensionName != null) {
                            File rootPath = getRootPath(configVirtualFile);
//                            String replace = extensionName.replaceFirst("_", "/");
                            String channel = configVirtualFile.getParent().getParent().getParent().getParent().getName();
                            Extension extension = new Extension(extensionName, channel, _project);
                            extension.setChannel(rootPath.getName());
                            extensionArrayList.add(extension);
                        }
                    }
                }
            }
        }

        Collections.sort(extensionArrayList, new Comparator<Extension>() {
            public int compare(Extension A00, Extension A01) {
                return A00.toString().compareTo(A01.toString());
            }
        });

        return extensionArrayList;
    }


    public File getRootPath(VirtualFile configVirtualFile) {
        File file = new File(configVirtualFile.getPath());
        while (!isRoot(file, true)) {
            file = file.getParentFile();
        }
        return file;
    }

    public static void main(String args[]) {

//        Magento magento = new Magento();
//        magento.getRootPath("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper/Data.php");

    }

    public ConfigXmlUtil.Rewrite[] getRewriteExtension(VirtualFile myFile) {
        ConfigXmlUtil.Rewrite[] rewrites = ConfigXmlUtil.getRewrites(myFile);

        String fileType = getFileType(new File(myFile.getPath()));

        List<ConfigXmlUtil.Rewrite> hasRewrite = new ArrayList<ConfigXmlUtil.Rewrite>();
        for (ConfigXmlUtil.Rewrite rewrite : rewrites) {
            if (rewrite.getRewriteType().equals(fileType.toLowerCase())) {
                hasRewrite.add(rewrite);
            }
        }
        return hasRewrite.toArray(new ConfigXmlUtil.Rewrite[hasRewrite.size()]);
    }

    public File getPathByExtensionName(String name, String channel) {
        String[] split = StringUtil.split(name, '_');
        if (split.length == 2) {
            String targetPath = channel + File.separator + split[0] + File.separator + split[1];
            Collection<VirtualFile> configVirtualFiles = FilenameIndex.getVirtualFilesByName(_project, "config.xml", GlobalSearchScopes.directoryScope(_project, _project.getBaseDir(), true));
            for (VirtualFile configVirtualFile : configVirtualFiles) {
                if (configVirtualFile.getPath().contains(targetPath + File.separator + "etc" + File.separator)) {
                    VirtualFile parent = configVirtualFile.getParent().getParent();
                    return new File(parent.getPath());
                }
            }
        }
        return null;
    }
}
