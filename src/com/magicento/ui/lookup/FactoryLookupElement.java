package com.magicento.ui.lookup;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementPresentation;
import com.kodokux.mageidea.Icons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class FactoryLookupElement extends LookupElement {

    private String uri;
    private String className;
    private static final Icon myIcon = Icons.MAGENTO_ICON_16;

    public FactoryLookupElement(String serviceId, String className) {
        this.uri = serviceId;
        this.className = className;
    }

    @NotNull
    @Override
    public String getLookupString() {
        return uri;
    }

    public void renderElement(LookupElementPresentation presentation) {
        presentation.setItemText(getLookupString());
        presentation.setTypeText(className);
        presentation.setTypeGrayed(true);
        presentation.setIcon(myIcon);
    }
}
