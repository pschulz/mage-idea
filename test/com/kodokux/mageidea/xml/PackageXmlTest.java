package com.kodokux.mageidea.xml;

import com.intellij.testFramework.UsefulTestCase;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * User: johna1203
 * Date: 2013/10/11
 * Time: 1:23
 */
public class PackageXmlTest extends UsefulTestCase {

    private File rootPath = null;
    private File packageFile;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        URL resource = PackageXmlTest.class.getResource("/resources/ECGKodokux_Voucher-1.0.1.6");
        rootPath = new File(resource.getPath());
        packageFile = new File(rootPath.getPath() + "/package2.xml");


    }

    @Test
    public void getDirType() throws Exception {
        PackageXml packageXml = new PackageXml(rootPath, packageFile);

        assertInstanceOf(packageXml.getDocument(), org.jdom.Document.class);

        String dirType = packageXml.getDirType("/app/code/community/ECGikenJp/I18n/Model/Design/Package.php");
        assertEquals(dirType, "magecommunity");

        dirType = packageXml.getDirType("/app/code/core/ECGikenJp/I18n/Model/Design/Package.php");
        assertEquals(dirType, "magecore");

        dirType = packageXml.getDirType("/app/code/local/ECGikenJp/I18n/Model/Design/Package.php");
        assertEquals(dirType, "magelocal");

        dirType = packageXml.getDirType("/app/design/frontend/base/default/layout/ecgpaygent/checkout.xml");
        assertEquals(dirType, "magedesign");

        dirType = packageXml.getDirType("/app/etc/modules/ECGikenJp_Catalog.xml");
        assertEquals(dirType, "mageetc");

        dirType = packageXml.getDirType("/lib/ECGKodokux/Validade/HankakuEisuKana.php");
        assertEquals(dirType, "magelib");

        dirType = packageXml.getDirType("/app/locale/ja_JP/ECGiken_Price.csv");
        assertEquals(dirType, "magelocale");

        dirType = packageXml.getDirType("/media/xmlconnect/original/ok.gif");
        assertEquals(dirType, "magemedia");

        dirType = packageXml.getDirType("/skin/frontend/base/default/debug/img/back.png");
        assertEquals(dirType, "mageskin");

        dirType = packageXml.getDirType("/tests/ECGikenJp/I18n/Model/Design/Package.php");
        assertEquals(dirType, "magetests");

        dirType = packageXml.getDirType("/js/ecgkodokux.js");
        assertEquals(dirType, "mageweb");

        dirType = packageXml.getDirType("/shell/test.php");
        assertEquals(dirType, "other");

        dirType = packageXml.getDirType("/johnatest/ecgkodokux.js");
        assertEquals(true, dirType.equals("mageweb") || dirType.equals("other"));

    }


//    @Test
//    public void testGetTargetElement() throws Exception {
//        PackageXml packageXml = new PackageXml(rootPath, packageFile);
//
//        Element el = packageXml.getTargetElement("magecommunity");
//
//        assertInstanceOf(el, org.jdom.Element.class);
//        assertEquals(el.getName(), "target");
//        assertEquals(el.getAttribute("name").getValue(), "magecommunity");
//
//
//        el = packageXml.getTargetElement("magedesign");
//        assertInstanceOf(el, org.jdom.Element.class);
//        assertEquals(el.getName(), "target");
//        assertEquals(el.getAttribute("name").getValue(), "magedesign");
//
//    }

    @Test
    public void testCreatePackage() throws Exception {
        PackageXml packageXml = new PackageXml(rootPath, packageFile);
        packageXml.setNameTag("johna_module");
        packageXml.setVersionTag("0.9.0");
        packageXml.setStabilityTag("stable");
        packageXml.setLicenseTag("BSD 2-Clause License", "http://opensource.org/licenses/BSD-2-Clause");
        packageXml.setChannelTag("community");
        packageXml.setSummaryTag("ECGKodokux_Voucher");
        packageXml.setDescriptionTag("じょなさん手sつ尾");
        packageXml.setDateTag("2013-09-06");
        packageXml.setTimeTag("15:32:01");
        packageXml.createPackage();

        packageXml.save(new File(rootPath, "testCreatePackage.xml"));

    }

    @Test
    public void testAddFile() throws Exception {

        PackageXml packageXml = new PackageXml(rootPath, packageFile);

        packageXml.addFile("app/code/community/ECGikenJp/I18n/Model/Design/Package.php");

//        packageXml.displayXml();

//
//        String dirType = packageXml.getDirType("/app/code/community/ECGikenJp/I18n/Model/Design/Package.php");
//        assertEquals(dirType, "magecommunity");
//
//        String dirType = packageXml.getDirType("/app/code/core/ECGikenJp/I18n/Model/Design/Package.php");
//        assertEquals(dirType, "magecore");

    }

    @Test
    public void testGetDocument() throws Exception {
        PackageXml packageXml = new PackageXml(rootPath, packageFile);
        assertInstanceOf(packageXml.getDocument(), org.jdom.Document.class);
    }
}
