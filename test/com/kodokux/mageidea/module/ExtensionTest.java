package com.kodokux.mageidea.module;

import com.intellij.openapi.project.Project;
import com.yourkit.util.Asserts;
import org.jdom.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/24
 * Time: 19:47
 */
public class ExtensionTest {
    private Extension extension;

    private String rootPath = "/Users/johna/PhpstormPluginProjects/bugathon_march_2013/app/code/core";
    private File rootPathFile;

    @Before
    public void setUp() throws Exception {

        Project project = Mockito.mock(Project.class);
        rootPathFile = new File(rootPath);
        extension = new Extension("Mage_Catalog", "core", project);
    }

    @Test
    public void testGetRootPath() throws Exception {
        File rootPath1 = extension.getRootPath();
        Asserts.assertEqual(rootPath1, rootPathFile);
    }

    @Test
    public void testSetChannel() throws Exception {
//        Asserts.assertEqual("community", rootPathFile);
    }

    @Test
    public void testGetConfigFile() throws Exception {
        File configFile = new File("/Users/johna/PhpstormPluginProjects/bugathon_march_2013/app/code/core/Mage/Catalog/etc/config.xml");
        Asserts.assertEqual(configFile, extension.getConfigFile());
    }

    @Test
    public void testGetConfigDocument() throws Exception {
        assertThat(extension.getConfigDocument(), instanceOf(Document.class));
    }

    @Test
    public void testGetExtensionModelTagName() throws Exception {
        Asserts.assertEqual(extension.getExtensionModelTagName(), "catalog");
    }

    @Test
    public void testGetNameByConfig() throws Exception {
        Asserts.assertEqual(extension.getExtensionName(), "Mage_Catalog");
        Asserts.assertEqual(extension.getNameByConfig(), "Mage_Catalog");
    }
}

